///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01e - Crazy Cat Lady - EE 205 - Spr 2022
///
/// @file    crazyCatLady.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o crazyCatLady crazyCatLady.cpp
///
/// Usage:  crazyCatLady
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ summation 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author Maxwell Pauly  <mgpauly@hawaii.edu>
/// @date    11 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>

int main( int argc, char* argv[] ) {
   std::string name = argv[1];
   std::cout << "Crazy Cat Lady!" << std::endl ;
   std::cout << "Oooooh! " << name << " you're so cute!" << std::endl;
   return 0;
}
