CC=g++

TARGET = crazyCatLady

all: $(TARGET)

crazyCatLady: crazyCatLady.cpp
	$(CC) -o $(TARGET) crazyCatLady.cpp

clean:
	rm -f $(TARGET) *.o
